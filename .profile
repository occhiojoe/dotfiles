#!bin/sh
#	~/.profile
#
# profile script
#

echo "running .profile..."

# add custom scripts to PATH
PATH=~/bin:$PATH

# add colors (nvim)
export TERM=xterm-256color

# set default editor
export EDITOR=nvim
export VISUAL=nvim
export GIT_EDITOR=nvim

# bash history
export HISTCONTROL=ignoredups

# default env
source $HOME/.env_custom
# secret env
source $HOME/.env_secret
# aliases
source $HOME/.aliases

# colorls
source $(dirname $(gem which colorls))/tab_complete.sh

# log
uptime
