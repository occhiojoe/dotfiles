# Joe's Dotfiles

Dotfiles for a simple bash or zsh shell setup!
Currently supports, osx, ubuntu, and manjaro

### Installing

Clone the repository

```
git clone git@gitlab.com:occhiojoe/dotfiles.git
```

run bin/init

```
./bin/init
```

Logout of user session and log back in, you should be good to go!

## Compatible With

* [Bash](https://www.gnu.org/software/bash/) - The Bourne Again SHell
* [Oh My Zsh](http://ohmyz.sh/) - The Zsh Shell

## Authors

* **Joseph Occhionero** - [check me out](https://gitlab.com/occhiojoe)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

