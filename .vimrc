"
"	~/.vimrc
"
" All Configs For Vim
"


" --- Setting Defaults --- "
syntax enable					" syntax highlighting baby!
filetype indent on				" load filetype-specific indent files

set tabstop=4					" number of visual spaces per TAB
set softtabstop=4 noexpandtab	" number of spaces in tab when editing (noexpandtab means no spaces)
set shiftwidth=4 smarttab

set number relativenumber		" set number in gutter to be relative
set cursorline					" highlight current line
set cursorcolumn				" highlight current column
set showmatch					" highlight matching [{()}]
set incsearch					" search as characters are entered
set hlsearch					" highlight matches

set foldenable					" enable folding

set path+=**					" find all files regardless of pwd
set showcmd						" show command in bottom bar
set wildmode=list:longest,full	" allow autocomplete in native vim commands
set splitbelow splitright		" show splits in the way specified
set wildmenu					" visual autocomplete for command menu
set lazyredraw					" redraw only when we need to.
set autochdir					" set working dir to current file being editted
set encoding=UTF-8

" --- Mappings --- "
" opening/editing vimrc
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" quick change to next window vim
nnoremap <space> :wincmd w<enter>

" --- Abbrevs --- "
iabbrev ssig -- <cr>Joe Occhionero<cr>

" --- Adding [Neo]vim plugins --- "
call plug#begin('~/.vim/plugged')
" defaults
Plug 'airblade/vim-gitgutter'           " git highlighting
Plug 'craigemery/vim-autotag'			" ctags autotagger
Plug 'ctrlpvim/ctrlp.vim'				" fuzzy file search
Plug 'gko/vim-coloresque'				" color highlighter
Plug 'junegunn/seoul256.vim'			" vim IDE color
Plug 'scrooloose/nerdtree'				" file explorer
Plug 'vim-airline/vim-airline'			" info bar for bottom of vim
Plug 'jreybert/vimagit'					" vim with git niceities
Plug 'duggiefresh/vim-easydir'			" create dirs easily
Plug 'tpope/vim-commentary'				" comments in vim
Plug 'tpope/vim-surround'				" surrounds things
Plug 'ryanoasis/vim-devicons'			" for NERD icons
" syntax highlighting
Plug 'sheerun/vim-polyglot'				" syntax highlighting
Plug 'pearofducks/ansible-vim'
Plug 'chrisbra/csv.vim'
Plug 'ekalinin/Dockerfile.vim'
Plug 'tpope/vim-git'
Plug 'fatih/vim-go'
Plug 'vim-scripts/groovy.vim'
Plug 'towolf/vim-helm'
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'elzr/vim-json'
Plug 'MTDL9/vim-log-highlighting'
Plug 'chr4/nginx.vim'
Plug 'StanAngeloff/php.vim'
Plug 'vim-python/python-syntax'
Plug 'rust-lang/rust.vim'
Plug 'vim-scripts/svg.vim'
Plug 'hashivim/vim-terraform'
Plug 'ericpruitt/tmux.vim'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'posva/vim-vue'
Plug 'vim-scripts/XSLT-syntax'
Plug 'amadeus/vim-xml'
Plug 'stephpy/vim-yaml'
" ale
Plug 'dense-analysis/ale'				" code checking


call plug#end()

" --- Config for Ale --- "
let g:ale_sign_column_always = 1			" git gutter
let g:airline#extensions#ale#enabled = 1	" airline

" --- Config for Seoule256 --- "
let g:seoul256_background = 236
colo seoul256
set background=dark

" --- Config for CtrlP --- "
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/vendor,*/node_modules
let g:ctrlp_show_hidden = 1
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'

" --- Config for Nerd --- "
function! StartUp()
    if 0 == argc()
        NERDTree
    end
endfunction
autocmd VimEnter * call StartUp()
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeShowHidden=1

" --- ColorHighlight> 80 --- "
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 80)
